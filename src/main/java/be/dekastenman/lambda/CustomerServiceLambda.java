package be.dekastenman.lambda;

import be.dekastenman.config.CustomerServiceComponent;
import be.dekastenman.config.DaggerCustomerServiceComponent;
import be.dekastenman.handler.APIGatewayProxyRequestEventHandler;
import be.dekastenman.handler.HealthCheckHandler;
import be.dekastenman.handler.UnsupportedHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import javax.inject.Inject;
import java.util.List;

public class CustomerServiceLambda {
    private CustomerServiceComponent customerServiceComponent;
    @Inject
    public List<APIGatewayProxyRequestEventHandler> handlers;
    @Inject
    public UnsupportedHandler unsupportedHandler;

    public CustomerServiceLambda() {
        initializeLambda();
    }

    protected void initializeLambda() {
        customerServiceComponent = DaggerCustomerServiceComponent.builder().build();
        customerServiceComponent.inject(this);
    }

    public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent apiGatewayRequest, Context context) {
        APIGatewayProxyRequestEventHandler handler = handlers.stream()
                .filter(h -> h.supports(apiGatewayRequest))
                .findFirst()
                .orElse(unsupportedHandler);

        return handler.handle(apiGatewayRequest, context);
    }
}
