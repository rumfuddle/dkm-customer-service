package be.dekastenman.converter;

import be.dekastenman.model.Address;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AddressConverter implements DynamoDBTypeConverter<String, Address> {
    private final Gson gson = new GsonBuilder().create();

    @Override
    public String convert(Address address) {
        return gson.toJson(address);
    }

    @Override
    public Address unconvert(String s) {
        return gson.fromJson(s, Address.class);
    }
}
