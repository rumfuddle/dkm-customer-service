package be.dekastenman.converter;

import be.dekastenman.model.Company;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CompanyConverter implements DynamoDBTypeConverter<String, Company> {
    private final Gson gson = new GsonBuilder().create();

    @Override
    public String convert(Company company) {
        return gson.toJson(company);
    }

    @Override
    public Company unconvert(String s) {
        return gson.fromJson(s, Company.class);
    }
}
