package be.dekastenman.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Company {
    private String name;
    private String vatNumber;
}
