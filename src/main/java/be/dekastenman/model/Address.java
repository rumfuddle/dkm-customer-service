package be.dekastenman.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Address {
    private String street;
    private String houseNumber;
    private String busNumber;
    private String city;
    private String zip;
    private Country country;
}
