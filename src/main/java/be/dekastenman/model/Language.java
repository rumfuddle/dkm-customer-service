package be.dekastenman.model;

public enum Language {
    DUTCH,
    FRENCH,
    GERMAN,
    ENGLISH
}
