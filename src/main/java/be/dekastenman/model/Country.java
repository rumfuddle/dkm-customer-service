package be.dekastenman.model;

public enum Country {
    BE,
    NL,
    FR,
    LUX,
    DE,
    UK
}
