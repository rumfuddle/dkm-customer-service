package be.dekastenman.model;

public enum CustomerType {
    PRIVATE,
    COMPANY
}
