package be.dekastenman.model;

import be.dekastenman.converter.AddressConverter;
import be.dekastenman.converter.CompanyConverter;
import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@DynamoDBTable(tableName = "customers")
public class Customer {
    @DynamoDBHashKey
    private String id;
    @DynamoDBTypeConvertedEnum
    @DynamoDBAttribute
    private CustomerType customerType;
    @DynamoDBTypeConvertedEnum
    @DynamoDBAttribute
    private Language language;
    @DynamoDBAttribute
    private String title;
    @DynamoDBAttribute
    private String firstName;
    @DynamoDBAttribute
    private String lastName;
    @DynamoDBTypeConverted(converter = CompanyConverter.class)
    @DynamoDBAttribute
    private Company company;
    @DynamoDBAttribute
    private String telephoneNumber;
    @DynamoDBAttribute
    private String email;
    @DynamoDBTypeConverted(converter = AddressConverter.class)
    @DynamoDBAttribute
    private Address invoiceAddress;
    @DynamoDBTypeConverted(converter = AddressConverter.class)
    @DynamoDBAttribute
    private Address deliveryAddress;
}
