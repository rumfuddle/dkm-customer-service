package be.dekastenman.service;

import be.dekastenman.dao.CustomerDAO;
import be.dekastenman.exception.CustomerAlreadyExistsException;
import be.dekastenman.exception.CustomerNotFoundException;
import be.dekastenman.model.Customer;

import java.util.List;

public class CustomerService {
    private final CustomerDAO customerDAO;

    public CustomerService(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    public Customer readCustomer(String id){
        return customerDAO.readCustomer(id);
    }

    public List<Customer> listCustomers() {
        return customerDAO.listCustomers();
    }

    public void createCustomer(Customer customer) throws CustomerAlreadyExistsException {
        customerDAO.createCustomer(customer);
    }

    public void updateCustomer(Customer customer) throws CustomerNotFoundException {
        customerDAO.updateCustomer(customer);
    }

    public void deleteCustomer(String id) {
        customerDAO.deleteCustomer(id);
    }
}
