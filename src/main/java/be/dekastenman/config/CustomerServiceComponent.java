package be.dekastenman.config;

import be.dekastenman.handler.*;
import be.dekastenman.lambda.CustomerServiceLambda;
import dagger.Component;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@Component(modules = {CustomerServiceModule.class})
public interface CustomerServiceComponent {
    List<APIGatewayProxyRequestEventHandler> provideHandlers();

    UnsupportedHandler provideUnsupportedHandler();

    void inject(CustomerServiceLambda lambda);
}
