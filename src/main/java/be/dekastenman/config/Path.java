package be.dekastenman.config;

public class Path {
    public static final String BASE_PATH = "\\/customer-api\\/v1";
    public static final String READ_CUSTOMER = BASE_PATH + "\\/customers\\/[0-9]+$";
    public static final String LIST_CUSTOMERS = BASE_PATH + "\\/customers$";
    public static final String CREATE_CUSTOMER = BASE_PATH + "\\/customers$";
    public static final String UPDATE_CUSTOMER = BASE_PATH + "\\/customers\\/[0-9]+$";
    public static final String DELETE_CUSTOMER = BASE_PATH + "\\/customers\\/[0-9]+$";
}
