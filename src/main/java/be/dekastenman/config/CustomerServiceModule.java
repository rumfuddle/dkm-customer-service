package be.dekastenman.config;

import be.dekastenman.dao.CustomTableNameResolver;
import be.dekastenman.dao.CustomerDAO;
import be.dekastenman.handler.*;
import be.dekastenman.service.CustomerService;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.Array;
import java.util.*;

@Module
public class CustomerServiceModule {
    public static final Map<String, String> DEFAULT_RESPONSE_HEADERS = Collections.unmodifiableMap(
            new HashMap() {{
                this.put("Content-Type", "application/json");
                this.put("Access-Control-Allow-Origin", "*");
            }}
    );

    @Singleton
    @Provides
    public Gson gson() {
        return new GsonBuilder().create();
    }

    @Singleton
    @Provides
    AmazonDynamoDB dynamoDBClient() {
        String endpoint = System.getenv("ENDPOINT_OVERRIDE");

        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard();
        if (!StringUtils.isNullOrEmpty(endpoint)) {
            builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, "eu-central-1"));
        }

        return builder.build();
    }

    @Singleton
    @Provides
    public DynamoDBMapper dynamoDBMapper(AmazonDynamoDB dynamoDBClient) {
        DynamoDBMapperConfig.Builder mapperConfigBuilder = DynamoDBMapperConfig
                .builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .withTableNameResolver(new CustomTableNameResolver());
        return new DynamoDBMapper(dynamoDBClient, mapperConfigBuilder.build());
    }

    @Singleton
    @Provides
    public CustomerDAO customerDAO(DynamoDBMapper dynamoDBMapper) {
        return new CustomerDAO(dynamoDBMapper);
    }

    @Singleton
    @Provides
    public CustomerService customerService(CustomerDAO customerDAO) {
        return new CustomerService(customerDAO);
    }

    @Singleton
    @Provides
    public UnsupportedHandler unsupportedHandler() {
        return new UnsupportedHandler();
    }

    @Singleton
    @Provides
    public List<APIGatewayProxyRequestEventHandler> handlers(CustomerService customerService, Gson gson) {
        List<APIGatewayProxyRequestEventHandler> handlers = new ArrayList<>();
        handlers.add(new ReadCustomerHandler(customerService, gson));
        handlers.add(new CreateCustomerHandler(customerService, gson));
        handlers.add(new HealthCheckHandler(gson));
        return handlers;
    }
}
