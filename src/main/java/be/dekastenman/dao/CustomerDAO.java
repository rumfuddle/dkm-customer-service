package be.dekastenman.dao;

import be.dekastenman.exception.CustomerAlreadyExistsException;
import be.dekastenman.exception.CustomerNotFoundException;
import be.dekastenman.model.Customer;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDeleteExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CustomerDAO {
    private static final String FIELD_NAME = "id";
    private final DynamoDBMapper dynamoDBMapper;

    public CustomerDAO(DynamoDBMapper dynamoDBMapper) {
        this.dynamoDBMapper = dynamoDBMapper;
    }

    public Customer readCustomer(String id) {
        return Optional
                .ofNullable(dynamoDBMapper.load(Customer.class, id))
                .orElseThrow(() -> new CustomerNotFoundException("customer with id " + id + " does not exist"));
    }

    public List<Customer> listCustomers() {
        return dynamoDBMapper.scan(Customer.class, new DynamoDBScanExpression());
    }

    public void createCustomer(Customer customer) {
        DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression()
                .withExpected(Collections.singletonMap(FIELD_NAME, new ExpectedAttributeValue(false)));

        try {
            dynamoDBMapper.save(customer, saveExpression);
        } catch (ConditionalCheckFailedException e) {
            throw new CustomerAlreadyExistsException("customer " + customer.getId() + " already exists");
        }
    }

    public void updateCustomer(Customer customer) {
        DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression()
                .withExpected(Collections.singletonMap(FIELD_NAME, new ExpectedAttributeValue(new AttributeValue(customer.getId()))));

        try {
            dynamoDBMapper.save(customer, saveExpression);
        } catch (ConditionalCheckFailedException e) {
            throw new CustomerNotFoundException("customer with id " + customer.getId() + " does not exist");
        }
    }

    public void deleteCustomer(String id) {
        Customer customer = new Customer();
        customer.setId(id);

        DynamoDBDeleteExpression deleteExpression = new DynamoDBDeleteExpression()
                .withExpected(Collections.singletonMap(FIELD_NAME, new ExpectedAttributeValue(new AttributeValue(id))));

        try {
            dynamoDBMapper.delete(customer, deleteExpression);
        } catch (ConditionalCheckFailedException e) {
            throw new CustomerNotFoundException("customer with id " + customer.getId() + " does not exist");
        }
    }
}
