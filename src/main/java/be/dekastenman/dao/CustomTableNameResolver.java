package be.dekastenman.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.util.StringUtils;

public class CustomTableNameResolver extends DynamoDBMapperConfig.DefaultTableNameResolver {
    @Override
    public String getTableName(Class<?> clazz, DynamoDBMapperConfig config) {
        String simpleTableName = super.getTableName(clazz, config);
        String suffix = System.getenv("DYNAMODB_TABLE_SUFFIX");
        return StringUtils.isNullOrEmpty(suffix) ? simpleTableName : simpleTableName + "-" + suffix;
    }
}
