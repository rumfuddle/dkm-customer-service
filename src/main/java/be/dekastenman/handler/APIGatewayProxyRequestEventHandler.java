package be.dekastenman.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

public interface APIGatewayProxyRequestEventHandler {
    default boolean supports(APIGatewayProxyRequestEvent apiGatewayRequest) {
        return false;
    };

    APIGatewayProxyResponseEvent handle(APIGatewayProxyRequestEvent apiGatewayRequest, Context context);
}
