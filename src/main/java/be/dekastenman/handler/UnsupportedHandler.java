package be.dekastenman.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.apache.http.HttpStatus;

import java.util.HashMap;

public class UnsupportedHandler implements APIGatewayProxyRequestEventHandler {
    @Override
    public boolean supports(APIGatewayProxyRequestEvent apiGatewayRequest) {
        return true;
    }

    @Override
    public APIGatewayProxyResponseEvent handle(APIGatewayProxyRequestEvent apiGatewayRequest, Context context) {
        return new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_BAD_REQUEST).withBody("invalid request").withHeaders(new HashMap<>());
    }
}
