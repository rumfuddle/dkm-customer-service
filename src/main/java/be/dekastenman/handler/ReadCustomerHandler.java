package be.dekastenman.handler;

import be.dekastenman.config.Path;
import be.dekastenman.model.Customer;
import be.dekastenman.model.ErrorMessage;
import be.dekastenman.service.CustomerService;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.kms.model.NotFoundException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static be.dekastenman.config.CustomerServiceModule.DEFAULT_RESPONSE_HEADERS;

@Slf4j
public class ReadCustomerHandler implements APIGatewayProxyRequestEventHandler {
    private static final String PARAMETER_NAME = "id";
    private final CustomerService customerService;
    private final Gson gson;

    @Inject
    public ReadCustomerHandler(CustomerService customerService, Gson gson) {
        this.customerService = customerService;
        this.gson = gson;
    }

    @Override
    public boolean supports(APIGatewayProxyRequestEvent apiGatewayRequest) {
        Matcher m = Pattern.compile(Path.READ_CUSTOMER).matcher(apiGatewayRequest.getPath());
        return HttpMethod.GET.name().equals(apiGatewayRequest.getHttpMethod()) && m.matches();
    }

    @Override
    public APIGatewayProxyResponseEvent handle(APIGatewayProxyRequestEvent apiGatewayRequest, Context context) {
        APIGatewayProxyResponseEvent apiGatewayResponse;
        try {
            Map<String, String> pathParameters = apiGatewayRequest.getPathParameters();
            String id = pathParameters.get(PARAMETER_NAME);

            Customer customer = customerService.readCustomer(id);

            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_OK).withBody(gson.toJson(customer)).withHeaders(DEFAULT_RESPONSE_HEADERS);
        } catch (NotFoundException e) {
            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_NOT_FOUND).withBody(gson.toJson(new ErrorMessage(e.getMessage()))).withHeaders(DEFAULT_RESPONSE_HEADERS);
        } catch (Exception e) {
            log.error("error reading customer", e);
            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).withBody(gson.toJson(new ErrorMessage(e.getMessage()))).withHeaders(DEFAULT_RESPONSE_HEADERS);
        }

        return apiGatewayResponse;
    }
}
