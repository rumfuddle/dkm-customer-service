package be.dekastenman.handler;

import be.dekastenman.config.Path;
import be.dekastenman.exception.CustomerAlreadyExistsException;
import be.dekastenman.model.Customer;
import be.dekastenman.model.ErrorMessage;
import be.dekastenman.service.CustomerService;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static be.dekastenman.config.CustomerServiceModule.DEFAULT_RESPONSE_HEADERS;

@Slf4j
public class CreateCustomerHandler implements APIGatewayProxyRequestEventHandler {
    private final CustomerService customerService;
    private final Gson gson;

    @Inject
    public CreateCustomerHandler(CustomerService customerService, Gson gson) {
        this.customerService = customerService;
        this.gson = gson;
    }

    @Override
    public boolean supports(APIGatewayProxyRequestEvent apiGatewayRequest) {
        Matcher m = Pattern.compile(Path.CREATE_CUSTOMER).matcher(apiGatewayRequest.getPath());
        return HttpMethod.POST.name().equals(apiGatewayRequest.getHttpMethod()) && m.matches();
    }

    @Override
    public APIGatewayProxyResponseEvent handle(APIGatewayProxyRequestEvent apiGatewayRequest, Context context) {
        APIGatewayProxyResponseEvent apiGatewayResponse;

        try {
            Customer customer = gson.fromJson(apiGatewayRequest.getBody(), Customer.class);

            //validator.validate(customer);
            customerService.createCustomer(customer);

            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_CREATED).withHeaders(DEFAULT_RESPONSE_HEADERS);
        } catch (JsonSyntaxException | ValidationException e) {
            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_BAD_REQUEST).withBody(gson.toJson(new ErrorMessage(e.getMessage()))).withHeaders(DEFAULT_RESPONSE_HEADERS);
        } catch (CustomerAlreadyExistsException e) {
            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_FORBIDDEN).withBody(gson.toJson(new ErrorMessage(e.getMessage()))).withHeaders(DEFAULT_RESPONSE_HEADERS);
        } catch (Exception e) {
            log.error("error creating customer", e);
            apiGatewayResponse = new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).withBody(gson.toJson(new ErrorMessage(e.getMessage()))).withHeaders(DEFAULT_RESPONSE_HEADERS);
        }

        return apiGatewayResponse;
    }
}
