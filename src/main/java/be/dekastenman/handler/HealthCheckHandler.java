package be.dekastenman.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Slf4j
public class HealthCheckHandler implements APIGatewayProxyRequestEventHandler {
    private static final String HEALTH_ENDPOINT_SUFFIX = "/info";
    private Gson gson;

    @Inject
    public HealthCheckHandler(Gson gson) {
        this.gson = gson;
    }

    @Override
    public boolean supports(APIGatewayProxyRequestEvent apiGatewayRequest) {
        return "GET".equals(apiGatewayRequest.getHttpMethod()) && HEALTH_ENDPOINT_SUFFIX.equals(apiGatewayRequest.getPath());
    }

    @Override
    public APIGatewayProxyResponseEvent handle(APIGatewayProxyRequestEvent apiGatewayRequest, Context context) {
        String body;
        try {
            Manifest manifest = null;
            Enumeration<URL> resources = getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");
            while (resources.hasMoreElements()) {
                manifest = new Manifest(resources.nextElement().openStream());
            }
            if (manifest == null) {
                body = "{\"message\":\"No manifest found\"}";
            }
            body = gson.toJson(manifest.getMainAttributes());
        } catch (IOException e) {
            log.error("Unreadable manifest", e);
            body = "{\"message\":\"Unreadable manifest\"}";
        }

        return new APIGatewayProxyResponseEvent().withStatusCode(HttpStatus.SC_OK).withBody(body).withHeaders(new HashMap<>());
    }
}
