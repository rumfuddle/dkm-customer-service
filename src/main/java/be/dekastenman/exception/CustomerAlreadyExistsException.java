package be.dekastenman.exception;

public class CustomerAlreadyExistsException extends IllegalArgumentException {
    public CustomerAlreadyExistsException(String message) {
        super(message);
    }
}
